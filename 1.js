// Soal nomor 1
const changeWord = (selectedText, changedText, text) => {
  let kalimatBaru = text.replace(selectedText, changedText);
  return kalimatBaru;
};
const kalimat1 = "Semoga kita selalu menjadi sebuah kisah klasik untuk masa depan";
const kalimat2 = "Izinkan aku merindumu walau dalam diam dan doa";

console.log(changeWord("kisah klasik", "mimpi yang terwujud", kalimat1));
console.log(changeWord("merindumu", "mengenangmu", kalimat2));
