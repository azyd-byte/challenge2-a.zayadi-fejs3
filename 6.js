// Soal omor 6
function getAngkaTerbesarKedua(personName) {
  if (Array.isArray(personName)) {
    personName.sort(function (a, b) {
      return a - b;
    });
    personName.reverse();
    return personName[1];
  } else {
    if (!personName) {
      return "ERROR: tidak ada input value";
    }
    return "ERROR: parameter bukan array";
  }
}

const dataAngka = [19, 4, 7, 7, 4, 3, 2, 100, 16, 13, 57, 34, 8];

console.log(getAngkaTerbesarKedua(dataAngka));
console.log(getAngkaTerbesarKedua("test"));
console.log(getAngkaTerbesarKedua());
