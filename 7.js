// Soal nomor 7
const dataPenjualanPakAldi = [
  {
    namaProduct: "Sepatu Futsal Nike Vapor Academy 8",
    hargaSatuan: 760000,
    kategori: "Sepatu Sport",
    totalPenjualan: 90,
  },
  {
    namaProduct: "Sepatu Warrior Trisan Black Brown High",
    hargaSatuan: 960000,
    kategori: "Sepatu Sneaker",
    totalPenjualan: 37,
  },
  {
    namaProduct: "Sepatu Warrior Trisan Maroon High",
    hargaSatuan: 360000,
    ategori: "Sepatu Sneaker",
    totalPenjualan: 90,
  },
  {
    namaProduct: "Sepatu Warrior Rainbow Tosca Corduroy",
    hargaSatuan: 120000,
    ategori: "Sepatu Sneaker",
    totalPenjualan: 90,
  },
];

const getTotalPenjualan = (dataPenjualan) => {
  if (Array.isArray(dataPenjualan)) {
    let error = [];
    let hitung = 0;
    let totalPenjualan = dataPenjualan.map((o) => {
      // jika data bukan number
      if (isNaN(o.hargaSatuan) || isNaN(o.totalPenjualan)) {
        // push data ke dalam value error
        error.push(o);
      } else {
        hitung = o.totalPenjualan;
        return hitung;
      }
    });
    // jika error ada isi nya
    if (error.length === 0) {
      // menjumlahkan nilai
      const reducer = (accumulator, num) => accumulator + num;
      return totalPenjualan.reduce(reducer);
    }
  } else {
    return "ERROR: parameter bukan array";
  }
};

console.log(getTotalPenjualan(dataPenjualanPakAldi));
