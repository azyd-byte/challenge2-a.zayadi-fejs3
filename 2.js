// Soal nomor 2
const checkTypeNumber = (givenNumber) => {
  let tipeAngka;
  if (Number.isInteger(givenNumber)) {
    if (givenNumber % 2 === 1) {
      return (tipeAngka = "GANJIL");
    }
    if (givenNumber % 2 === 0) {
      return (tipeAngka = "GENAP");
    }
  } else {
    if (!givenNumber) {
      return (tipeAngka = "ERROR: Where is the parameter Bro???");
    }
    return (tipeAngka = "Error = Invalid data type ");
  }
};

console.log(checkTypeNumber(57));
console.log(checkTypeNumber(86));
console.log(checkTypeNumber("3"));
console.log(checkTypeNumber({}));
console.log(checkTypeNumber([]));
console.log(checkTypeNumber());
