const dataPenjualanNovel = [
  {
    idProduct: "Book002421",
    namaProduct: "Pulang - Pergi",
    penulis: "Tere Liye",
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStok: 17,
  },
  {
    idProduct: "Book002351",
    namaProduct: "Selamat Tinggal",
    penulis: "Tere Liye",
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStok: 20,
  },
  {
    idProduct: "Book002941",
    namaProduct: "Garis Waktu",
    penulis: "Fiersa Besari",
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStok: 5,
  },
  {
    idProduct: "Book002941",
    namaProduct: "Laskar Pelangi",
    penulis: "Andrea Hirata",
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStok: 56,
  },
];

const compare = (a, b) => {
  if (a.totalTerjual > b.totalTerjual) {
    return -1;
  }
  if (a.totalTerjual < b.totalTerjual) {
    return 1;
  }
  return 0;
};

const getInfoPenjualan = (dataPenjualan) => {
  let totalKeuntungan = 0;
  let totalModal = 0;
  let totalPenjualan = 0;
  let persentaseKeuntungan = 0;
  let penulis = [];

  dataPenjualan.map((o) => {
    //mencari keuntungan
    totalKeuntungan = totalKeuntungan + (o.hargaJual - o.hargaBeli) * o.totalTerjual;

    //mencari total modal
    totalModal = totalModal + o.hargaBeli * (o.totalTerjual + o.sisaStok);

    // mencari total penjualan
    totalPenjualan = totalPenjualan + o.hargaJual * o.totalTerjual;

    // mencari % keuntungan
    persentaseKeuntungan = (totalPenjualan / totalModal) * 100;

    // mencari nama penulis untuk dijumlahkan
    let findPenulis = penulis.findIndex((obj) => obj.nama == o.penulis);
    if (findPenulis != -1) {
      penulis[findPenulis].totalTerjual = penulis[findPenulis].totalTerjual + o.totalTerjual;
    } else {
      penulis.push({ nama: o.penulis, totalTerjual: o.totalTerjual });
    }
  });

  // sorting penjualan buku terlaris
  dataPenjualan.sort(compare);
  // console.log(dataPenjualan);

  // sorting penulis buku terlaris
  penulis.sort(compare);
  // console.log(penulis);

  // titik pemisah ribuan
  const numberWithCommas = (x) => {
    let str = x?.toString()?.replace(/,/g, "");
    if (str) {
      return str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else {
      return null;
    }
  };

  return {
    totalKeuntungan: "RP. " + numberWithCommas(totalKeuntungan),
    totalModal: "RP. " + numberWithCommas(totalModal),
    persentaseKeuntungan: persentaseKeuntungan.toFixed(2) + "%",
    produkBukuTerlaris: dataPenjualan[0].namaProduct,
    penulisTerlaris: penulis[0].nama,
  };
};

console.log(getInfoPenjualan(dataPenjualanNovel));
